﻿using System;
using System.Windows;

namespace PiFilter
{
    /// <summary>
    /// Interaktionslogik für Disconnected.xaml
    /// </summary>
    public partial class Disconnected : Window
    {
        MainWindow context;

        /*
         *  Notifies the user about a lost connection
         *  @Context    refers to the MainWindow, to clean up some values for shutdown
         */
        public Disconnected(MainWindow Context)
        {
            InitializeComponent();
            this.context = Context;
            context.client = null;
            this.context.InputPrevention.Visibility = Visibility.Visible;
        }

        /*
         *  Hard exit
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (context.client == null)
            {
                Environment.Exit(0);
            }
        }
    }
}
