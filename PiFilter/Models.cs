﻿namespace Models
{
    public class sos
    {
        public biquad[] biquads;
        public int rows;
    }
    public class InputProperties
    {
        public int Type;
        public double lWn;
        public double hWn;
        public double n;
    }
    public class biquad
    {
        public string a0;
        public string a1;
        public string a2;
        public string b0;
        public string b1;
        public string b2;
    }
}
