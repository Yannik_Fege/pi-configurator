﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace PiFilter
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MqttClient client;

        private int enabled;
        private enum Types : int
        {
            lowpass = 0,    // 0
            highpass,       // 1
            bandpass,       // 2
            bandstop        // 3
        }

        public MainWindow()
        {
            EstablishConnection();
            client.ConnectionClosed += ConnectionLost;
            InitializeComponent();

            Subscribe();
            Init_FilterOptions();

            this.DataContext = this;
        }

        /*
         *  Establishes a connection with an MQTT Broker
         */
        private void EstablishConnection()
        {
            PiFilter.Connect connect = new PiFilter.Connect(this);
            connect.ShowDialog();

            try
            {
                client.Connect(Guid.NewGuid().ToString());

                if (!client.IsConnected)
                {
                    EstablishConnection();
                }
            }
            catch
            {
                EstablishConnection();
            }
        }

        /*
         *  Adds all available filters to the Combo Box selection
         */
        private void Init_FilterOptions()
        {
            foreach (var filter in Enum.GetNames(typeof(Types)))
            {
                FilterSelection.Items.Add(filter);
            }
        }

        /*
         *  Handles a lost Connection
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void ConnectionLost(object sender, EventArgs e)
        {
            Dispatcher.Invoke((Action)delegate ()
            {
                PiFilter.Disconnected disconnected = new PiFilter.Disconnected(this);
                disconnected.ShowDialog();
            });
        }

        /*
         *  Pushes new configuration to the MQTT service
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void WriteVariables(object sender, RoutedEventArgs e)
        {
            Types selectedFilter;
            double noChar;
            double vlWn;
            double vhWn;

            Enum.TryParse(FilterSelection.Text, out selectedFilter);

            if ((Double.TryParse(lWn.Text, out vlWn) && Double.TryParse(hWn.Text, out vhWn) && Double.TryParse(n.Text, out noChar)))
            {
                if (((int)selectedFilter < 2) || ((int)selectedFilter >= 2 && vlWn < vhWn))
                {
                    client.Publish(
                        "/InputProperties",
                        Encoding.ASCII.GetBytes($"{{\"Type\": {(int)selectedFilter}, \"lWn\": {lWn.Text.Replace(',', '.')}, \"hWn\": {hWn.Text.Replace(',', '.')}, \"n\": {n.Text.Replace(',', '.')}}}"),
                        MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE,
                        true
                        );
                }

            }
        }

        /*
         *  Toggles the filter on and off
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void Toggle(object sender, RoutedEventArgs e)
        {
            ToggleHelper(enabled == 1 ? false : true);
        }
        private void ToggleHelper(bool enable)
        {
            client.Publish(
                "/enabled",
                Encoding.ASCII.GetBytes(enable ? "1" : "0"),
                MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE,
                true
                );
        }

        /*
         *  Sets button color depending on state in MQTT
         */
        private void ToggleConfirm()
        {
            ToggleFilter.Background = (enabled == 0 ? Brushes.Red : Brushes.Green);
        }

        /*
         *  Subscribes the MQTT client to all necessary topics
         */
        private void Subscribe()
        {
            string[] topics = { "/coef", "/InputProperties", "/enabled" };
            byte[] qos = { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE };

            client.MqttMsgPublishReceived += SubscriptionHandler;
            ushort subscriptionToken = client.Subscribe(topics, qos);
        }

        /*
         *  Handles an incoming MQTT publish event and acts accordingly
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        void SubscriptionHandler(object sender, MqttMsgPublishEventArgs e)
        {
            Dispatcher.Invoke((Action)delegate ()
            {
                string message = Encoding.UTF8.GetString(e.Message).Replace("inf", $"{Double.MaxValue}"); ;
                try
                {
                    switch (e.Topic)
                    {
                        case "/coef":
                            Models.sos coefficients = JsonConvert.DeserializeObject<Models.sos>(message);
                            string output = "Coefficient Values: \n";
                            for (int i = 0; i < coefficients.biquads.Length; i++)
                            {
                                output += "Biquad " + i + "\n";
                                output += "a0: " + "\t" + coefficients.biquads[i].a0 + "\n";
                                output += "a1: " + "\t" + coefficients.biquads[i].a1 + "\n";
                                output += "a2: " + "\t" + coefficients.biquads[i].a2 + "\n";
                                output += "b0: " + "\t" + coefficients.biquads[i].b0 + "\n";
                                output += "b1: " + "\t" + coefficients.biquads[i].b1 + "\n";
                                output += "b2: " + "\t" + coefficients.biquads[i].b2 + "\n";
                                output += "\n";
                            }
                            output += "rows: " + "\t" + coefficients.rows + "\n";
                            CoeffOut.Text = output;
                            break;
                        case "/InputProperties":
                            List<TextBox> propertyInputs = new List<TextBox> { lWn, hWn, n };
                            Models.InputProperties inputProperties = JsonConvert.DeserializeObject<Models.InputProperties>(message);
                            FilterSelection.Text = ((Types)inputProperties.Type).ToString();
                            lWn.Text = inputProperties.lWn.ToString().Replace(',', '.');
                            hWn.Text = inputProperties.hWn.ToString().Replace(',', '.');
                            n.Text = inputProperties.n.ToString().Replace(',', '.');
                            break;
                        case "/enabled":
                            enabled = Int32.Parse(message);
                            ToggleConfirm();
                            break;
                    }
                }
                catch
                {
                    ToggleHelper(false);
                }
            });
        }

        /*
         *  Makes sure the program shuts down cleanly
         *  @e          Arguments passed by the event
         */
        protected override void OnClosed(EventArgs e)
        {
            if (client != null)
            {
                client.Disconnect();
            }

            base.OnClosed(e);
            App.Current.Shutdown();
        }
    }
}