﻿using System;
using System.Net;
using System.Windows;
using uPLibrary.Networking.M2Mqtt;

namespace PiFilter
{
    /// <summary>
    /// Interaktionslogik für Connect.xaml
    /// </summary>
    public partial class Connect : Window
    {
        MainWindow context;

        /*
         *  Handles creation of a connection to an MQTT broker
         *  @Context    refers to MainWindow to set values
         */
        public Connect(MainWindow Context)
        {
            this.context = Context;
            InitializeComponent();
        }

        /*
         *  Loops connection attempts and checks for correct input
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void MQTTConnect(object sender, RoutedEventArgs e)
        {
            if (IPInput.Text != null)
            {
                IPAddress connectIP = null;
                if (IPAddress.TryParse(IPInput.Text, out connectIP))
                {
                    // create reader
                    context.client = new MqttClient(connectIP);

                    Close();
                }
                else
                {
                    IPInput.Text = "";
                }
            }
        }

        /*
         *  Hard Exit
         *  @sender     Event sender
         *  @e          Arguments passed by the event
         */
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (context.client == null)
            {
                Environment.Exit(0);
            }
        }
    }
}
